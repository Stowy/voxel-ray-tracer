#ifndef STW_RAY_TRACING
#define STW_RAY_TRACING

#include "math.glsl"
#include "random.glsl"
#include "uniforms.glsl"

const int GRID_SIZE = 256;
const int GRID_SIZE_2 = GRID_SIZE * GRID_SIZE;
const int GRID_SIZE_3 = GRID_SIZE * GRID_SIZE * GRID_SIZE;
const int MAX_RAY_STEPS = GRID_SIZE_2;
const float RAY_START_OFFSET_ALONG_NORMAL = 1.5;
// const float PER_PIXEL_NORMAL_NUDGE = 0.5;
const int MAX_BOUNCES = 8;

struct FindResult
{
	uint material_index;
	ivec3 voxel_coord;
	vec3 collision_point;
	bool is_inside_voxel;
	float hit_distance;
};

struct TraceResult
{
	vec3 color;
	bool hit_something;
	vec3 primary_hit_position;
};

FindResult FindNearestVoxel(Ray ray);
bool IsOutsideCanvas(ivec3 coord);
vec3 ComputeVoxelNormal(ivec3 coord);

uint Sample(ivec3 pos)
{
	int signed_index = pos.x + pos.y * GRID_SIZE + pos.z * GRID_SIZE_2;
	if (signed_index < 0 || signed_index >= GRID_SIZE_3) return 0;

	uint index = uint(signed_index);
	uint packed_index = index / 4;
	uint sub_index = index % 4;
	
	uint packed_material_index = push_constants.voxel_buffer.voxels[packed_index];
	uint material_index = (packed_material_index >> (sub_index * 8)) & 0xff;

	return material_index;
}

vec3 CosWeightedDiffuseReflection(vec3 normal, inout uint rng_state)
{
	return normalize(normal + RandomUnitVector(rng_state));
}

float FresnelReflectAmount(float refraction_current, float refraction_hit, vec3 normal, vec3 incident, float f0, float f90)
{
	// Schlick aproximation
	float r0 = (refraction_current - refraction_hit) / (refraction_current + refraction_hit);
	r0 *= r0;
	float cosX = -dot(normal, incident);
	if (refraction_current > refraction_hit)
	{
		float n = refraction_current / refraction_hit;
		float sinT2 = n * n * (1.0 - cosX * cosX);
		// Total internal reflection
		if (sinT2 > 1.0)
			return f90;
		cosX = sqrt(1.0 - sinT2);
	}
	float x = 1.0 - cosX;
	float ret = r0 + (1.0 - r0) * x * x * x * x * x;

	// adjust reflect multiplier for object reflectivity
	return mix(f0, f90, ret);
}

TraceResult PathTraceRay(Ray primary_ray, vec2 pixel_sample_position)
{
	Ray current_ray = primary_ray;
	vec3 throughput = vec3(1.0);

	TraceResult trace_result = TraceResult(vec3(0.0), false, vec3(0.0));
	
	for (int bounce_index = 0; bounce_index < MAX_BOUNCES; ++bounce_index)
	{
		FindResult find_result = FindNearestVoxel(current_ray);

		if (find_result.material_index == 0)
		{
			float u = atan( current_ray.direction.z, current_ray.direction.x ) * INV2PI;
			float v = acos( current_ray.direction.y ) * INVPI;
			vec3 env_map_value = textureLod(environement_map, vec2(u, v), 0).rgb;
			trace_result.color += throughput * SRGBToLinear(env_map_value);
			return trace_result;
		}

		if (!trace_result.hit_something)
		{
			trace_result.primary_hit_position = find_result.collision_point;
		}
		trace_result.hit_something = true;

		VoxelMaterial material = push_constants.material_buffer.materials[find_result.material_index];
		vec3 base_color = SRGBToLinear(material.base_color);
		float roughness = material.roughness;
		vec3 normal = material.normal;
		if (normal == vec3(0.0)) normal = ComputeVoxelNormal(find_result.voxel_coord);
		vec3 emissive = material.emissive;
		float ior = material.ior;
		vec3 specular_color = material.specular_color;
		float percent_specular = material.specular_percent;

		vec3 voxel_center = vec3(find_result.voxel_coord) + 0.5;
		vec3 ray_origin = voxel_center + normal * RAY_START_OFFSET_ALONG_NORMAL;
		vec3 ray_dir_per_voxel = normalize(ray_origin - current_ray.origin);

		if (percent_specular > 0.0)
		{
			float start_ior = find_result.is_inside_voxel ? ior : 1.0;
			float end_ior = !find_result.is_inside_voxel ? 1.0 : ior;
			percent_specular = FresnelReflectAmount(start_ior, end_ior, normal, ray_dir_per_voxel, percent_specular, 1.0);
		}

		vec3 brdf = base_color / PI;

		uint rng_state = uint(uint(find_result.voxel_coord.x) * uint(1973) + 
			uint(find_result.voxel_coord.y) * uint(6043) + 
			uint(find_result.voxel_coord.z) * uint(9277) + 
			uint(float(push_constants.accumulated_frame_count) + push_constants.time * 10000.0) * uint(26699)) | uint(1);

		float ray_probability = 1.0;
		float do_specular = 0.0;
		float ray_select_roll = RandomFloat01(rng_state);

		if (percent_specular > 0.0 && ray_select_roll < percent_specular)
		{
			do_specular = 1.0;
			ray_probability = percent_specular;
		}
		else
		{
			ray_probability = 1.0 - percent_specular;
		}

		ray_probability = max(ray_probability, 0.001);

		vec3 diffuse_ray_direction = CosWeightedDiffuseReflection(normal, rng_state);

		vec3 specular_ray_direction = reflect(ray_dir_per_voxel, normal);
		// Squaring the roughness is just a convention to make roughness feel more linear perceptually
		specular_ray_direction = normalize(mix(specular_ray_direction, diffuse_ray_direction, roughness * roughness));

		vec3 new_direction = mix(diffuse_ray_direction, specular_ray_direction, do_specular);

		float probability_density_function = dot(normal, new_direction) / PI;

		trace_result.color += emissive * throughput;
		throughput *= brdf * (dot(normal, new_direction) / probability_density_function);
		throughput /= ray_probability;

		// Russian Roulette
		// As the throughput gets smaller, the ray is more likely to get terminated early
		// Survivors have their value boosted to make up for fewer samples being in the average
		if (ubo.is_russian_roulette_enabled) {
			float p = max(throughput.r, max(throughput.g, throughput.b));
			if (RandomFloat01(rng_state) > p)
				break;
		
			// Add the energy we 'lose' by randomly terminating paths
			throughput *= 1.0 / p;
		}

		if (find_result.is_inside_voxel)
		{
			break;
		}
		else
		{
			current_ray.origin = ray_origin;
			current_ray.direction = new_direction;
		}
	}

	return TraceResult(vec3(0.0), false, vec3(0.0));
}

TraceResult PathTraceRayPerPixel(Ray primary_ray, vec2 pixel_sample_position)
{
	Ray current_ray = primary_ray;
	vec3 throughput = vec3(1.0);

	TraceResult trace_result = TraceResult(vec3(0.0), false, vec3(0.0));
	
	for (int bounce_index = 0; bounce_index < MAX_BOUNCES; ++bounce_index)
	{
		FindResult find_result = FindNearestVoxel(current_ray);

		if (find_result.material_index == 0)
		{
			float u = atan( current_ray.direction.z, current_ray.direction.x ) * INV2PI;
			float v = acos( current_ray.direction.y ) * INVPI;
			vec3 env_map_value = textureLod(environement_map, vec2(u, v), 0).rgb;
			trace_result.color += throughput * SRGBToLinear(env_map_value);
			return trace_result;
		}

		if (!trace_result.hit_something)
		{
			trace_result.primary_hit_position = find_result.collision_point;
		}
		trace_result.hit_something = true;

		VoxelMaterial material = push_constants.material_buffer.materials[find_result.material_index];
		vec3 base_color = SRGBToLinear(material.base_color);
		float roughness = material.roughness;
		vec3 normal = material.normal;
		if (normal == vec3(0.0)) normal = ComputeVoxelNormal(find_result.voxel_coord);
		vec3 emissive = material.emissive;
		float ior = material.ior;
		vec3 specular_color = material.specular_color;
		float percent_specular = material.specular_percent;

		vec3 ray_origin = find_result.collision_point + normal * RAY_START_OFFSET_ALONG_NORMAL;

		if (percent_specular > 0.0)
		{
			float start_ior = find_result.is_inside_voxel ? ior : 1.0;
			float end_ior = find_result.is_inside_voxel ? 1.0 : ior;
			percent_specular = FresnelReflectAmount(start_ior, end_ior, normal, current_ray.direction, percent_specular, 1.0);
		}

		vec3 brdf = base_color / PI;

		uint rng_state = uint(uint(pixel_sample_position.x) * uint(1973) + 
			uint(pixel_sample_position.y) * uint(6043) + 
			uint(float(push_constants.accumulated_frame_count) + push_constants.time * 10000.0) * uint(26699)) | uint(1);

		float ray_probability = 1.0;
		float do_specular = 0.0;
		float ray_select_roll = RandomFloat01(rng_state);

		if (percent_specular > 0.0 && ray_select_roll < percent_specular)
		{
			do_specular = 1.0;
			ray_probability = percent_specular;
		}
		else
		{
			ray_probability = 1.0 - percent_specular;
		}

		ray_probability = max(ray_probability, 0.001);

		vec3 diffuse_ray_direction = CosWeightedDiffuseReflection(normal, rng_state);

		vec3 specular_ray_direction = reflect(current_ray.direction, normal);
		// Squaring the roughness is just a convention to make roughness feel more linear perceptually
		specular_ray_direction = normalize(mix(specular_ray_direction, diffuse_ray_direction, roughness * roughness));

		vec3 new_direction = mix(diffuse_ray_direction, specular_ray_direction, do_specular);

		float probability_density_function = dot(normal, new_direction) / PI;

		trace_result.color += emissive * throughput;
		throughput *= brdf * (dot(normal, new_direction) / probability_density_function);
		throughput /= ray_probability;

		// Russian Roulette
		// As the throughput gets smaller, the ray is more likely to get terminated early
		// Survivors have their value boosted to make up for fewer samples being in the average
		if (ubo.is_russian_roulette_enabled) {
			float p = max(throughput.r, max(throughput.g, throughput.b));
			if (RandomFloat01(rng_state) > p)
				break;
		
			// Add the energy we 'lose' by randomly terminating paths
			throughput *= 1.0 / p;
		}

		current_ray.origin = ray_origin;
		current_ray.direction = new_direction;
	}

	return TraceResult(vec3(0.0), false, vec3(0.0));
}

FindResult FindNearestVoxel(Ray ray)
{
	FindResult result;
	result.material_index = 0;

	if (ray.direction.x == 0.0) ray.direction.x = 0.0001;
	if (ray.direction.y == 0.0) ray.direction.y = 0.0001;
	if (ray.direction.z == 0.0) ray.direction.z = 0.0001;

	vec3 ray_sign_float = sign(ray.direction);
	ivec3 ray_sign = ivec3(ray_sign_float);
	vec3 ray_inv_direction = 1.0 / ray.direction;
	// vec3 step_size = 1.0 / abs(ray.direction);
	vec3 step_size = ray_sign_float / ray.direction;

	vec3 ray_origin_grid = floor(ray.origin);
	ivec3 voxel_coords = ivec3(ray_origin_grid);

	vec3 side_distance = ray_origin_grid - ray.origin;
	side_distance += 0.5;
	side_distance += ray_sign_float * 0.5;
	side_distance *= ray_inv_direction;

	vec3 axis_to_step_to = vec3(0);

	for (int i = 0; i < MAX_RAY_STEPS; i++)
	{
		if (IsOutsideCanvas(voxel_coords)) return result;

		uint material_index = Sample(voxel_coords);
		if (material_index != 0)
		{
			result.material_index = material_index;

			result.hit_distance = length(axis_to_step_to * (side_distance - step_size));
			result.collision_point = ray.origin + ray.direction * result.hit_distance;

			result.voxel_coord = voxel_coords;

			result.is_inside_voxel = i == 0;

			return result;
		}

		// All components of `axis_to_step_to` are 0,
		// EXCEPT for the corresponding largest component of side_distance,
		// which is the axis along which the ray should be incremented.
		axis_to_step_to = side_distance.x < side_distance.z ?
			 (side_distance.x < side_distance.y ? vec3(1, 0, 0) : vec3(0, 1, 0)) :
			 (side_distance.z < side_distance.y ? vec3(0, 0, 1) : vec3(0, 1, 0));

		side_distance += axis_to_step_to * step_size;
		voxel_coords += ivec3(axis_to_step_to) * ray_sign;
	}

	return result;
}

vec3 ComputeVoxelNormal(ivec3 coord)
{
	const int SAMPLES = 3;

	vec3 normal = vec3(0);
	for (int x = -SAMPLES; x <= SAMPLES; x++)
	{
		for (int y = -SAMPLES; y <= SAMPLES; y++)
		{
			for (int z = -SAMPLES; z <= SAMPLES; z++)
			{
				ivec3 offset = ivec3(x, y, z);
				if (offset != ivec3(0) && Sample(coord + offset) != 0) normal += offset;
			}
		}
	}

	if (abs(length(normal)) < 0.001) 
	{
		normal = vec3(0, -1, 0);
	}

	return -normalize(normal);
}


bool IsOutsideCanvas(ivec3 coord)
{
	bool x_is_outside = coord.x < 0 || coord.x >= GRID_SIZE;
	bool y_is_outside = coord.y < 0 || coord.y >= GRID_SIZE;
	bool z_is_outside = coord.z < 0 || coord.z >= GRID_SIZE;

	return x_is_outside || y_is_outside || z_is_outside;
}

#endif

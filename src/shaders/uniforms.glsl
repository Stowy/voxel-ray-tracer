// I want all my shader files to be able to read various uniform data but 
// I don't want to always include them bellow the code bellow in the .comp file
// so this file is here to solve this problem. Maybe it's a bad idea, but it should be simple enough to fix

#ifndef STW_PUSH_CONSTANTS
#define STW_PUSH_CONSTANTS

#include "camera.glsl"
#include "math.glsl"

layout(rgba16f, set = 0, binding = 0) uniform writeonly image2D image;
layout(set = 0, binding = 1) uniform sampler2D previous_image;
layout(set = 0, binding = 2) uniform sampler2D blue_noise;
layout(set = 0, binding = 4) uniform sampler2D environement_map;
layout(set = 0, binding = 5) uniform writeonly image2D ldr_image;
layout(std140, set = 0, binding = 3) uniform UniformBufferObjects
{
	vec4 light_position;
	vec4 light_color;
	vec4 last_frame_camera_position;
	ivec2 draw_image_size;
	ivec2 blue_noise_size;
	Frustum last_frame_frustum;
	bool is_russian_roulette_enabled;
	float padding;
	bool per_pixel_lighting;
} ubo;

layout(buffer_reference, std430) readonly buffer VoxelBuffer
{
	uint voxels[];
};

struct VoxelMaterial 
{
	vec3 base_color;
	float roughness;
	vec3 emissive;
	float ior;
	vec3 normal;
	float padding;
	vec3 specular_color;
	float specular_percent;
};

layout(buffer_reference, std430) readonly buffer MaterialBuffer
{
	VoxelMaterial materials[];
};

struct GpuCamera
{
	float aspect;
	float position_x;
	float position_y;
	float position_z;
	float target_x;
	float target_y;
	float target_z;
	float top_left_x;
	float top_left_y;
	float top_left_z;
	float top_right_x;
	float top_right_y;
	float top_right_z;
	float bottom_left_x;
	float bottom_left_y;
	float bottom_left_z;
};

layout(push_constant) uniform PushConstants
{
	// Camera data
	GpuCamera gpu_camera;
	float time;
	int accumulated_frame_count;
	VoxelBuffer voxel_buffer;
	MaterialBuffer material_buffer;
} push_constants;

// Helper function to hide the ugly truth
// If this ends up slowing stuff down, I'll remove it
Camera CameraFromGpuCamera(GpuCamera gpu_camera)
{
	Camera c;
	c.aspect = gpu_camera.aspect;

	c.position.x = gpu_camera.position_x;
	c.position.y = gpu_camera.position_y;
	c.position.z = gpu_camera.position_z;

	c.target.x = gpu_camera.target_x;
	c.target.y = gpu_camera.target_y;
	c.target.z = gpu_camera.target_z;

	c.top_left.x = gpu_camera.top_left_x;
	c.top_left.y = gpu_camera.top_left_y;
	c.top_left.z = gpu_camera.top_left_z;

	c.top_right.x = gpu_camera.top_right_x;
	c.top_right.y = gpu_camera.top_right_y;
	c.top_right.z = gpu_camera.top_right_z;

	c.bottom_left.x = gpu_camera.bottom_left_x;
	c.bottom_left.y = gpu_camera.bottom_left_y;
	c.bottom_left.z = gpu_camera.bottom_left_z;

	return c;
}

#endif
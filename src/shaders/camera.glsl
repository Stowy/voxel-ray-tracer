#ifndef STW_CAMERA
#define STW_CAMERA

#include "math.glsl"

struct Camera
{
	float aspect;
	vec3 position;
	vec3 target;
	vec3 top_left;
	vec3 top_right;
	vec3 bottom_left;
};

Ray GetPrimaryRayFromCamera(Camera camera, vec2 tex_coord, ivec2 screen_size)
{
	float u = tex_coord.x * (1.0 / float(screen_size.x));
	float v = tex_coord.y * (1.0 / float(screen_size.y));
	vec3 p = camera.top_left + u * (camera.top_right - camera.top_left) + v * (camera.bottom_left - camera.top_left);

	vec3 direction = normalize(p - camera.position);
	Ray ray = Ray(camera.position, direction/*, vec3(1.0), 0*/);
	return ray;
}

#endif // STW_CAMERA

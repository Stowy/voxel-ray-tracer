#ifndef STW_RAY
#define STW_RAY

const float INV2PI = 0.15915494309189533576888;
const float INVPI = 0.31830988618379067153777;
const float PI = 3.14159265359;
const float TWO_PI = 2.0 * PI;
// also just fract(goldenRatio)
const float GOLDEN_RATIO_CONJUGATE = 0.61803398875; 

struct Ray
{
	vec3 origin;
	vec3 direction;
};

struct Plane
{
	// w = distance_to_origin
	vec4 normal;
};

struct Frustum
{
	Plane planes[4];
};

float DistanceToPlane(Plane plane, vec3 point)
{
	float plane_distance_to_origin = plane.normal.w;
	return dot(plane.normal.xyz, point) - plane_distance_to_origin;
}

vec3 LessThan(vec3 f, float value)
{
    return vec3(
        (f.x < value) ? 1.0 : 0.0,
        (f.y < value) ? 1.0 : 0.0,
        (f.z < value) ? 1.0 : 0.0);
}
 
vec3 LinearToSRGB(vec3 rgb)
{
    rgb = clamp(rgb, 0.0, 1.0);
 
    return mix(
        pow(rgb, vec3(1.0 / 2.4)) * 1.055 - 0.055,
        rgb * 12.92,
        LessThan(rgb, 0.0031308)
    );
}
 
vec3 SRGBToLinear(vec3 rgb)
{
    rgb = clamp(rgb, 0.0f, 1.0f);
 
    return mix(
        pow(((rgb + 0.055f) / 1.055f), vec3(2.4f)),
        rgb / 12.92f,
        LessThan(rgb, 0.04045f)
    );
}

// ACES tone mapping curve fit to go from HDR to LDR
//https://knarkowicz.wordpress.com/2016/01/06/aces-filmic-tone-mapping-curve/
vec3 ACESFilm(vec3 x)
{
    float a = 2.51;
    float b = 0.03;
    float c = 2.43;
    float d = 0.59;
    float e = 0.14;
    return clamp((x * (a * x + b)) / (x * (c * x + d) + e), 0.0, 1.0);
}

vec3 Perpendicular(vec3 v)
{
    if (abs(v.x) > abs(v.z))
        return vec3(-v.y, v.x, 0.0);
    else
        return vec3(0.0, -v.z, v.y);
}

#endif

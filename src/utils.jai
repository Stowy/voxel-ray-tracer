#scope_module

// Return the length of s, a C-style zero-terminated string.
// If you pass in a pointer that is not zero-terminated, BAD things will happen!
strlen :: (s: *u8) -> s64 
{  
	count: s64 = 0;

	while << s {
		count += 1;
		s += 1;
	}

	return count;
}

strcmp :: (s1: *u8, s2: *u8) -> s32
{
	while (<<s1 != #char "\0" && (<<s1 == <<s2)) 
	{
		s1 += 1; 
		s2 += 1;
	}
	return (<<s1 - <<s2);
}

offset_of :: ($T: Type, ident: Code) -> s64 #expand 
{
	t: T = ---;
	return cast(*void) (*t.#insert ident) - cast(*void) *t;
}

// Perlin noise implementation - https://stackoverflow.com/questions/29711668/perlin-noise-generation

Noise :: (i: s32, x: s32, y: s32) -> float32
{
	n := x + y * 37;
	n = (n << 13) ^ n;

	a := PRIMES[i][0];
	b := PRIMES[i][1];
	c := PRIMES[i][2];

	t := (n * (n * n * a + b) + c) & 0x7fffffff;
	return 1.0 - cast(float32) t /  cast(float32) 1073741824.0;
}

SmoothedNoise :: (i: s32, x: s32, y: s32) -> float32
{
	corners := (Noise( i, x - 1, y - 1 ) + Noise(i, x + 1, y - 1 ) + Noise(i, x - 1, y + 1 ) + Noise( i, x + 1, y + 1 )) / 16;
	sides := (Noise( i, x - 1, y ) + Noise( i, x + 1, y ) + Noise( i, x, y - 1 ) + Noise( i, x, y + 1 )) / 8;
	center := Noise( i, x, y ) / 4;
	return corners + sides + center;
}

Interpolate :: (a: float32, b: float32, x: float32) -> float32
{
	ft := x * PI;
	f := (1.0 - cos(ft)) * 0.5;
	return a * (1.0 - f) + b * f;
}

InterpolatedNoise :: (i: s32, x: float32, y: float32) -> float32
{
	int_x := cast(s32) x;
	int_y := cast(s32) y;
	frac_x := x - int_x;
	frac_y := y - int_y;
	v1 := SmoothedNoise(i, int_x, int_y);
	v2 := SmoothedNoise(i, int_x + 1, int_y);
	v3 := SmoothedNoise(i, int_x, int_y + 1);
	v4 := SmoothedNoise(i, int_x + 1, int_y + 1);
	i1 := Interpolate(v1, v2, frac_x);
	i2 := Interpolate(v3, v4, frac_x);
	return Interpolate(i1, i2, frac_y);
}

Noise3D :: (x: float32, y: float32, z: float32) -> float32
{
	noise := 0.0;
	frequency := 5.0;
	amplitude := 0.5 / 6.0;
	for 0..NUM_OCTAVES - 1
	{
		i := cast(s32) it;
		noiseXY := InterpolatedNoise(i, x * frequency, y * frequency);
		noiseXZ := InterpolatedNoise(i, x * frequency, z * frequency);
		noiseYZ := InterpolatedNoise(i, y * frequency, z * frequency);

		noiseYX := InterpolatedNoise(i, y * frequency, x * frequency);
		noiseZX := InterpolatedNoise(i, z * frequency, x * frequency);
		noiseZY := InterpolatedNoise(i, z * frequency, y * frequency);

		noise += (noiseXY + noiseXZ + noiseYZ + noiseYX + noiseZX + noiseZY) * amplitude;
		amplitude *= PERSISTENCE;
		frequency *= 2.0;
	}

	return noise;
}

#scope_file

PRIMES :: ([3] s32).[
	s32.[995615039, 600173719, 701464987], 
	s32.[831731269, 162318869, 136250887], 
	s32.[174329291, 946737083, 245679977], 
	s32.[362489573, 795918041, 350777237], 
	s32.[457025711, 880830799, 909678923], 
	s32.[787070341, 177340217, 593320781], 
	s32.[405493717, 291031019, 391950901], 
	s32.[458904767, 676625681, 424452397], 
	s32.[531736441, 939683957, 810651871], 
	s32.[997169939, 842027887, 423882827]
];

NUM_OCTAVES :: 3;
PERSISTENCE :: 0.5;
